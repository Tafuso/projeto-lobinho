const urlParams = new URLSearchParams(window.location.search)
const sectionLobos = document.querySelector('.lobos')
const idAtualPage = parseInt(urlParams.get('page'))
var loboInvertido = false

const pegarListaLobos = () => {
  fetch(`https://lobinhos.herokuapp.com/wolves`)
    .then(res => res.json())
    .then(wolfs => {
      listWolves(wolfs, idAtualPage, 4)
    })
    .catch(() => alert("Ocorreu um erro"))
}

const pegarListaLobosAdotados = () => {
  fetch(`https://lobinhos.herokuapp.com/wolves/adopted`)
    .then(res => res.json())
    .then(wolfs => {
      listWolves(wolfs, idAtualPage, 4)
    })
    .catch(() => alert("Ocorreu um erro"))
}

const listWolves = (items, pageAtual, limiteItens) => {
  let result = []
  let totalPages = Math.ceil(items.length / limiteItens)
  let count = (pageAtual * limiteItens) - limiteItens
  let delimiter = count + limiteItens

  if (pageAtual <= totalPages) {
    for (let i = count; i < delimiter; i++) {
      result.push(items[i])
      count++
    }
  }
  createWolf(result, totalPages)
}

const createWolf = (wolfs, totalPages) => {
  sectionLobos.innerHTML = ``
  wolfs.forEach(wolf => {
    if (wolf != undefined && !wolf.adopted) {
      const div = document.createElement('div')
      loboInvertido ? div.className = 'lobo invertido' : div.className = 'lobo'
      div.innerHTML = `<img src="${wolf.image_url}" alt="imagem de lobo">
      <div class="detalhes">
        <h2>${wolf.name}</h2>
        <h3>Idade: ${wolf.age} anos</h3>
        <p>${wolf.description}</p>
        <a class="adotar-button" href="../pages/lobo-adote.html?lobo=${wolf.id}">Adotar</a>
      </div>`
      sectionLobos.appendChild(div)
      loboInvertido = !loboInvertido
    }
    else if (wolf != undefined && wolf.adopted) {
      const div = document.createElement('div')
      loboInvertido ? div.className = 'lobo invertido' : div.className = 'lobo'
      div.innerHTML = `<img src="${wolf.image_url}" alt="imagem de lobo">
      <div class="detalhes">
        <h2>${wolf.name}</h2>
        <h3>Idade: ${wolf.age} anos</h3>
        <p>${wolf.description}</p>
        <h4>Adotado por: ${wolf.adopter_name}</h4>
        <button class="adotado-button">Adotado</button>
      </div>`
      sectionLobos.appendChild(div)
      loboInvertido = !loboInvertido
    }

  })

  const ul = document.createElement('ul')
  ul.className = "pagination"
  ul.innerHTML = `
    <li>
      <a href="/pages/listar-lobos.html?page=1">
        ←
      </a>
    </li>
    <li>
      <a href="/pages/listar-lobos.html?page=${idAtualPage - 2}">
        ${idAtualPage - 2}
      </a>
    </li>    
    <li>
      <a href="/pages/listar-lobos.html?page=${idAtualPage - 1}">
        ${idAtualPage - 1}
      </a>
    </li>
    <li class='page-atual'>
      ${idAtualPage}
    </li>
    
    <li>
      <a href="/pages/listar-lobos.html?page=${idAtualPage + 1}">
        ${idAtualPage + 1}
      </a>
    </li>
    <li>
      <a href="/pages/listar-lobos.html?page=${idAtualPage + 2}">
        ${idAtualPage + 2}
      </a>
    </li>
    <li>
      <a href="/pages/listar-lobos.html?page=${totalPages}">
        →
      </a>
    </li>
  `
  sectionLobos.appendChild(ul)
}

pegarListaLobos()

const setarLista = (checked) => {
  checked ? pegarListaLobosAdotados() : pegarListaLobos()

}
